package com.mall.order.services;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.errorprone.annotations.Var;
import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.converter.OrderConverter;
import com.mall.order.dal.entitys.*;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dto.*;
import com.mall.order.utils.ExceptionProcessorUtils;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.utils.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 *  cskaoyan
 */
@Slf4j
@Component
@Service
public class OrderQueryServiceImpl implements OrderQueryService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderItemMapper orderItemMapper;

    @Autowired
    OrderShippingMapper orderShippingMapper;

    @Autowired
    OrderConverter orderConverter;

    /**
     * @description:此方法用于查询订单
     * @return: com.mall.order.dto.OrderListResponse
     * @date: 2022年1月20日16:43:05
     * @author ZhaoJun
     */
    @Override
    public OrderListResponse02 queryOrder(Long uid) {
        OrderListResponse02 orderListResponse = new OrderListResponse02();
        Example example = new Example(Order.class);
        example.createCriteria()
                .andEqualTo("userId",uid);
        List<Order> orders = orderMapper.selectByExample(example);
        int i = orderMapper.selectCountByExample(example);


        //获得Data
        List<OrderDetailInfo> orderDetailInfos1 = orderConverter.orders2details(orders);

        //获得OrderItemDto
        for (OrderDetailInfo orderDetailInfo : orderDetailInfos1) {
            Example example1 = new Example(OrderItem.class);
            example1.createCriteria()
                    .andEqualTo("orderId",orderDetailInfo.getOrderId());
            List<OrderItem> orderItems = orderItemMapper.selectByExample(example1);
            List<OrderItemDto> orderItemDtos = orderConverter.orderItems2ItemDTOs(orderItems);
            orderDetailInfo.setOrderItemDto(orderItemDtos);
        }

        //获得ShippingDto
        for (OrderDetailInfo orderDetailInfo : orderDetailInfos1) {
            Example example1 = new Example(OrderShipping.class);
            example1.createCriteria()
                    .andEqualTo("orderId",orderDetailInfo.getOrderId());
            List<OrderShipping> orderShippings = orderShippingMapper.selectByExample(example1);
            OrderShippingDto orderShippingDto = orderConverter.shipping2dto(orderShippings.get(0));
            //赋值
            orderDetailInfo.setOrderShippingDto(orderShippingDto);
        }
        orderListResponse.setData(orderDetailInfos1);
        orderListResponse.setTotal((long) i);

        return orderListResponse;
    }

    /**
     * @description:此方法用于查询订单详情
     * @return: com.mall.order.dto.OrderDetailResponse
     * @date: 2022年1月21日09:10:06
     * @author ZhaoJun
     */
    @Override
    public OrderDetailResponse queryOrderDetail(String orderId) {
        //查询订单表中的信息
        //普通成员变量
        Example example = new Example(Order.class);
        example.createCriteria()
                .andEqualTo("orderId",orderId);
        List<Order> orders = orderMapper.selectByExample(example);
        Order order = orders.get(0);

        //list成员变量
        Example example1 = new Example(OrderItem.class);
        example1.createCriteria()
                .andEqualTo("orderId",orderId);
        List<OrderItem> orderItems = orderItemMapper.queryByOrderId(orderId);
        List<OrderItemDto> orderItemDtos = orderConverter.item2dto(orderItems);

        //DTO成员变量
        Example example2 = new Example(OrderShipping.class);
        example1.createCriteria()
                .andEqualTo("orderId",orderId);
        List<OrderShipping> orderShippings = orderShippingMapper.selectByExample(example2);


        //给response赋值
        OrderDetailResponse orderDetailResponse = new OrderDetailResponse();
        orderDetailResponse.setOrderId(orderId);
        orderDetailResponse.setPayment(order.getPayment());
        orderDetailResponse.setPaymentType(order.getPaymentType());
        orderDetailResponse.setPostFee(order.getPostFee());
        orderDetailResponse.setStatus(order.getStatus());
        orderDetailResponse.setCreateTime(order.getCreateTime());
        orderDetailResponse.setUpdateTime(order.getUpdateTime());
        orderDetailResponse.setPaymentTime(order.getPaymentTime());
        orderDetailResponse.setConsignTime(order.getConsignTime());
        orderDetailResponse.setEndTime(order.getEndTime());
        orderDetailResponse.setCloseTime(order.getCloseTime());
        orderDetailResponse.setShippingName(order.getShippingName());
        orderDetailResponse.setShippingCode(order.getShippingCode());
        orderDetailResponse.setUserId(order.getUserId());
        orderDetailResponse.setBuyerMessage(order.getBuyerMessage());
        orderDetailResponse.setBuyerNick(order.getBuyerNick());
        orderDetailResponse.setBuyerComment(order.getBuyerComment());

        orderDetailResponse.setOrderItemDto(orderItemDtos);
        orderDetailResponse.setOrderShippingDto(orderConverter.shipping2dto(orderShippings.get(0)));

        orderDetailResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
        orderDetailResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());

        return orderDetailResponse;
    }
}
