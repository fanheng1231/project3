package com.mall.order.biz.handler;

import com.mall.commons.tool.exception.BizException;
import com.mall.commons.tool.utils.NumberUtils;
import com.mall.order.biz.callback.SendEmailCallback;
import com.mall.order.biz.callback.TransCallback;
import com.mall.order.biz.context.AbsTransHandlerContext;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.constants.OrderConstants;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dto.CartProductDto;
import com.mall.order.utils.GlobalIdGeneratorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 *
 * 初始化订单 生成订单
 */

@Slf4j
@Component
public class InitOrderHandler extends AbstractTransHandler {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private OrderShippingMapper orderShippingMapper;

    @Autowired
    GlobalIdGeneratorUtil globalIdGeneratorUtil;

    public String ORDER_ID_PREFIX = "order_id";
    public String ORDER_ITEM_ID_PREFIX = "order_item_id";

    @Override
    public boolean isAsync() {
        return false;
    }

    @Override
    public boolean handle(TransHandlerContext context) {
        CreateOrderContext createOrderContext = (CreateOrderContext) context;
        Order order = new Order();
        try {
            //插入订单表
            String orderId = globalIdGeneratorUtil.getNextSeq(ORDER_ID_PREFIX,1);
            order.setOrderId(orderId);
            order.setUserId(createOrderContext.getUserId());
            order.setBuyerNick(createOrderContext.getUserName());
            order.setPayment(createOrderContext.getOrderTotal());
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            order.setStatus(OrderConstants.ORDER_STATUS_INIT);
            orderMapper.insert(order);

            //插入订单商品关联表
            List<Long> buyProductIds = new ArrayList<>();
            List<CartProductDto> cartProductDtoList = createOrderContext.getCartProductDtoList();
            for (CartProductDto cartProductDto : cartProductDtoList) {
                OrderItem orderItem = new OrderItem();
                orderItem.setId(globalIdGeneratorUtil.getNextSeq(ORDER_ITEM_ID_PREFIX, 1));
                orderItem.setItemId(cartProductDto.getProductId());
                orderItem.setOrderId(orderId);
                orderItem.setNum(cartProductDto.getProductNum().intValue());
                orderItem.setPrice(cartProductDto.getSalePrice().doubleValue());
                orderItem.setTitle(cartProductDto.getProductName());
                orderItem.setPicPath(cartProductDto.getProductImg());
                //商品总价
                orderItem.setTotalFee(cartProductDto.getSalePrice().multiply(new BigDecimal(cartProductDto.getProductNum())).doubleValue());
                orderItem.setStatus(1);
                buyProductIds.add(cartProductDto.getProductId());
                orderItemMapper.insert(orderItem);

                createOrderContext.setOrderId(orderId);
                createOrderContext.setBuyProductIds(buyProductIds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //

        return true;
    }
}
