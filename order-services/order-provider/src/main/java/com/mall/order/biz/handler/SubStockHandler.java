package com.mall.order.biz.handler;

import com.mall.commons.tool.exception.BizException;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.dal.entitys.Stock;
import com.mall.order.dal.persistence.StockMapper;
import com.mall.order.dto.CartProductDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 扣减库存处理器
 * @Author： wz
 * @Date: 2019-09-16 00:03
 **/
@Component
@Slf4j
public class SubStockHandler extends AbstractTransHandler {

    @Autowired
    private StockMapper stockMapper;

	@Override
	public boolean isAsync() {
		return false;
	}

	@Override
	@Transactional
	public boolean handle(TransHandlerContext context) {
		//contest强转为createOrderContext
		CreateOrderContext createOrderContext = (CreateOrderContext) context;

		//取出商品信息
		List<CartProductDto> productDtoList = createOrderContext.getCartProductDtoList();

		//取出商品id
//		List<Long> productIds = createOrderContext.getBuyProductIds();
		List<CartProductDto> cartProductDtos = createOrderContext.getCartProductDtoList();
		List<Long> productIds = new ArrayList<>();
		for (CartProductDto cartProductDto : cartProductDtos) {
			Long productId = cartProductDto.getProductId();
			productIds.add(productId);
		}

		if (CollectionUtils.isEmpty(productIds)){
			for (CartProductDto cartProductDto : productDtoList) {
				productIds.add(cartProductDto.getProductId());
			}
		}
		//排序
		productIds.sort(Long::compareTo);

		//锁定库存
		List<Stock> stocksForUpdate = stockMapper.findStocksForUpdate(productIds);

		if(CollectionUtils.isEmpty(stocksForUpdate)){
			throw new BizException("库存未初始化!");
		}

		if(stocksForUpdate.size()!=productIds.size()){
			throw new BizException("部分商品库存未初始化!");
		}

		//扣减库存
		for (CartProductDto cartProductDto : productDtoList) {
			Long productId = cartProductDto.getProductId();
			Long productNum = cartProductDto.getProductNum();

			//productNum不能超出我们限购的数量

			Stock stock = new Stock();
			stock.setItemId(productId);
			stock.setLockCount(productNum.intValue());
			stock.setStockCount(-productNum);
			stockMapper.updateStock(stock);
		}
		return true;
	}
}