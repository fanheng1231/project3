package com.mall.order.biz.handler;

import com.mall.commons.tool.exception.BizException;
import com.mall.order.biz.context.AbsTransHandlerContext;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.constant.OrderRetCode;
import com.mall.shopping.ICartService;
import com.mall.shopping.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 将购物车中的缓存失效
 */
@Slf4j
@Component
public class ClearCartItemHandler extends AbstractTransHandler {

    @Reference(timeout = 3000,check = false)
    ICartService iCartService;

    //是否采用异步方式执行
    @Override
    public boolean isAsync() {
        return false;
    }

    @Override
    public boolean handle(TransHandlerContext context) {
        //获取userId
        CreateOrderContext createOrderContext = (CreateOrderContext) context;
        Long userId = createOrderContext.getUserId();

        //对request进行赋值
        CartListByIdRequest cartListByIdRequest = new CartListByIdRequest();
        cartListByIdRequest.setUserId(userId);
        CartListByIdResponse cartListById = iCartService.getCartListById(cartListByIdRequest);
        List<CartProductDto> cartProductDtos = cartListById.getCartProductDtos();
        List<Long> longs = new ArrayList<>();

        ClearCartItemRequest clearCartItemRequest = new ClearCartItemRequest();
        clearCartItemRequest.setUserId(userId);
        clearCartItemRequest.setProductIds(longs);

        for (CartProductDto cartProductDto : cartProductDtos) {
            longs.add(cartProductDto.getProductId());
        }

        iCartService.clearCartItemByUserID(clearCartItemRequest);
        return true;
    }
}
