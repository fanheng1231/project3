package com.mall.order.converter;

import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.entitys.OrderShipping;
import com.mall.order.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;

import java.util.List;

/**
 *  cskaoyan
 */
@Mapper(componentModel = "spring")
public interface OrderConverter {

    @Mappings({})
    List<OrderItemDto> orderItems2ItemDTOs(List<OrderItem> orderItems);

    @Mappings({})
    OrderDetailResponse order2res(Order order);

    @Mappings({})
    OrderDetailInfo order2detail(Order order);

    @Mappings({})
    OrderItemDto item2dto(OrderItem item);

    @Mappings({})
    OrderShippingDto shipping2dto(OrderShipping shipping);

    @Mappings({})
    OrderItemDtoDTO item2itemDto(OrderItem orderItem);

    List<OrderItemDto> item2dto(List<OrderItem> items);

    @Mappings({})
    OrderItemResponse item2res(OrderItem orderItem);

    @Mappings({})
    OrderShippingDtoDTO shipping2ShippingDtoDto(OrderShipping orderShipping);

    @Mappings({})
    OrderDto order2dto(Order order);

    @Mappings({})
    List<OrderDetailInfo> orders2details(List<Order> orders);
}
