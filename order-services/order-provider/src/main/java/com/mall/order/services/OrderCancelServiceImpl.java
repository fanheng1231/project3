package com.mall.order.services;

import com.mall.order.OrderCancelService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;
import lombok.extern.log4j.Log4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @description:
 * @author:Mr.Dai
 **/
@Service
@Component
@Log4j
public class OrderCancelServiceImpl implements OrderCancelService {
    @Autowired
    OrderMapper orderMapper;
    //此接口用于取消订单
    @Override@Transactional(isolation = Isolation.REPEATABLE_READ,propagation = Propagation.REQUIRES_NEW)
    public CancelOrderResponse cancelOrder(CancelOrderRequest request) {
        CancelOrderResponse response = new CancelOrderResponse();
        Date date = new Date();
        long timeStamp = date.getTime();
        try {
            request.requestCheck();
            String orderId = request.getOrderId();
            int affected = orderMapper.updateOrderInfo(orderId,date);
            response.setCode(OrderRetCode.SUCCESS.getCode());
            response.setMsg(OrderRetCode.SUCCESS.getMessage());
        } catch (NumberFormatException e) {
            response.setCode(OrderRetCode.DB_EXCEPTION.getCode());
            response.setMsg(OrderRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }
}
