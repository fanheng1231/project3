package com.mall.order.dal.persistence;

import com.mall.commons.tool.tkmapper.TkMapper;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.entitys.Stock;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderMapper extends TkMapper<Order> {
    Long countAll();

    int deleteOrderShippingInfo(@Param("orderId") String orderId);

    int deleteOrder(@Param("orderId") String orderId);

    List<OrderItem> selectOrderItemInfo(@Param("orderId") String orderId);

    List<Stock> selectStocksForUpdate(@Param("itemIds") List<Long> itemIds);

    void updateStockInfo(@Param("stock") Stock stock);

    int deleteOrderItemInfo(@Param("orderId") String orderId);

    int updateOrderInfo(@Param("orderId") String orderId, @Param("date") Date date);
}