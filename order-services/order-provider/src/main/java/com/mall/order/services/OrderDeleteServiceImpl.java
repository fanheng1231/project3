package com.mall.order.services;

import com.mall.commons.tool.exception.BizException;
import com.mall.order.OrderDeleteService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.entitys.Stock;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author:Mr.Dai
 **/
@Component
@Service
public class OrderDeleteServiceImpl implements OrderDeleteService {
    @Autowired
    OrderMapper orderMapper;

    //此接口用于删除订单
    @Override@Transactional(isolation = Isolation.REPEATABLE_READ)
    public DeleteOrderResponse deleteOrder(DeleteOrderRequest request) {
        DeleteOrderResponse response = new DeleteOrderResponse();
        Date date = new Date();
        try {
            request.requestCheck();
            //删除商品物流信息表数据
            int temp1 = orderMapper.deleteOrderShippingInfo(request.getOrderId());
            //删除订单表订单数据
            int temp2 = orderMapper.deleteOrder(request.getOrderId());

            //维护商品库存表数据
            List<OrderItem> orderItems = orderMapper.selectOrderItemInfo(request.getOrderId());
            List<Long> itemIds = new ArrayList<>();
            List<Integer> itemNumList = new ArrayList<>();
            for (OrderItem orderItem : orderItems) {
                itemIds.add(orderItem.getItemId());
                itemNumList.add(orderItem.getNum());
            }
            List<Stock> stocks = orderMapper.selectStocksForUpdate(itemIds);
            if(CollectionUtils.isEmpty(stocks)){
                throw new BizException("库存尚未初始化");
            }
            if(stocks.size()!=itemIds.size()){
                throw new BizException("部分库存尚未初始化");
            }
            for (OrderItem orderItem : orderItems) {
                Stock stock = new Stock();
                stock.setItemId(orderItem.getItemId());
                stock.setStockCount(orderItem.getNum().longValue());
                stock.setLockCount(-orderItem.getNum());
                orderMapper.updateStockInfo(stock);
            }

            //删除订单-商品表数据
              int temp3 = orderMapper.deleteOrderItemInfo(request.getOrderId());

            response.setCode(OrderRetCode.SUCCESS.getCode());
            response.setMsg(OrderRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            response.setCode(OrderRetCode.DB_EXCEPTION.getCode());
            response.setMsg(OrderRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }
}
