package com.mall.order.bootstrap;

import com.mall.order.OrderQueryService;
import com.mall.order.converter.OrderConverter;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.OrderDetailInfo;
import com.mall.order.dto.OrderListRequest;
import com.mall.order.dto.OrderListResponse;
import com.mall.order.utils.GlobalIdGeneratorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderProviderApplicationTests {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderConverter orderConverter;

    @Autowired
    OrderQueryService orderQueryService;

    public String ORDER_ID_PREFIX = "order_id";

    @Autowired
    GlobalIdGeneratorUtil globalIdGeneratorUtil;

    /**
     * @description:用于查询订单的测试方法
     * @return: void
     * @date: 2022年1月20日16:44:23
     * @author ZhaoJun
     */
    @Test
    public void contextLoads() {
//        OrderListRequest request = new OrderListRequest();
//        request.setUserId(62L);
//        OrderListResponse orderListResponse = new OrderListResponse();
//        Example example = new Example(Order.class);
//        example.createCriteria()
//                .andEqualTo(request.getUserId());
//        List<Order> orders = orderMapper.selectByExample(example);
//        List<OrderDetailInfo> orderDetailInfos = orderConverter.orders2details(orders);
//        orderListResponse.setDetailInfoList(orderDetailInfos);
//        Long aLong = orderMapper.countAll();
//        orderListResponse.setTotal(aLong);
//        System.out.println(orderListResponse);
    }

    @Test
    public void orderIdCreate(){
        String orderId = globalIdGeneratorUtil.getNextSeq(ORDER_ID_PREFIX,1);
        System.out.println(orderId);
    }

    @Test
    public void orderSelect(){
//        OrderListRequest orderListRequest = new OrderListRequest();
//        orderListRequest.setUserId(62L);
//        OrderListResponse orderListResponse = orderQueryService.queryOrder(orderListRequest);
//        System.out.println(orderListResponse.toString());
    }
}
