package com.mall.order.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @time: 2022/1/21 16:27
 * @author: Zhao Jun
 */
@NoArgsConstructor
@Data
public class DataDTO {
    private String orderId;
    private Integer payment;
    private Object paymentType;
    private Object postFee;
    private Integer status;
    private String createTime;
    private String updateTime;
    private Object paymentTime;
    private Object consignTime;
    private Object endTime;
    private Object closeTime;
    private Object shippingName;
    private Object shippingCode;
    private Integer userId;
    private Object buyerMessage;
    private String buyerNick;
    private Object buyerComment;
    private List<OrderItemDtoDTO> orderItemDto;
    private OrderShippingDtoDTO orderShippingDto;
}
