package com.mall.order.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @time: 2022/1/21 16:25
 * @author: Zhao Jun
 */
@NoArgsConstructor
@Data
public  class OrderShippingDtoDTO {
    private String orderId;
    private String receiverName;
    private String receiverPhone;
    private Object receiverMobile;
    private Object receiverState;
    private Object receiverCity;
    private Object receiverDistrict;
    private String receiverAddress;
    private Object receiverZip;
}

