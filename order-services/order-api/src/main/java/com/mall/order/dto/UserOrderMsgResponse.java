package com.mall.order.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @time: 2022/1/21 15:55
 * @author: Zhao Jun
 */
@NoArgsConstructor
@Data
public class UserOrderMsgResponse {

    private List<DataDTO> data;
    private Integer total;
}
