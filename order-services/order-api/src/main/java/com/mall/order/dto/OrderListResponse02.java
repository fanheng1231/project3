package com.mall.order.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**

 */
@Data
public class OrderListResponse02 implements Serializable {

    private List<OrderDetailInfo> data;

    /**
     * 总记录数
     */
    private Long total;

}
