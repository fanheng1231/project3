package com.mall.order;

import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;

/**
 * @description:
 * @author:Mr.Dai
 **/
//此接口用于删除订单
public interface OrderDeleteService {
    DeleteOrderResponse deleteOrder(DeleteOrderRequest request);
}
