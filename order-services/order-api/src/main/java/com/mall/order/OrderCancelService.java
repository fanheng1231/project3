package com.mall.order;

import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;

/**
 * @description:
 * @author:Mr.Dai
 **/
//此接口用于取消订单
public interface OrderCancelService {
    CancelOrderResponse cancelOrder(CancelOrderRequest request);
}
