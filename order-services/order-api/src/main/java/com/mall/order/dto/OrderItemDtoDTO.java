package com.mall.order.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @time: 2022/1/21 16:26
 * @author: Zhao Jun
 */
@NoArgsConstructor
@Data
public class OrderItemDtoDTO {
    private String id;
    private String itemId;
    private String orderId;
    private Integer num;
    private String title;
    private Integer price;
    private Integer totalFee;
    private String picPath;
}
