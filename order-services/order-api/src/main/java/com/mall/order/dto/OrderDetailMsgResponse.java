package com.mall.order.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @time: 2022/1/20 20:36
 * @author: Zhao Jun
 */
@NoArgsConstructor
@Data
public class OrderDetailMsgResponse extends AbstractResponse {
    private String userName;
    private Integer orderTotal;
    private Integer userId;
    private List<OrderItemDto> goodsList;
    private String tel;
    private String streetName;
    private Integer orderStatus;

}
