package com.mall.order;

import com.mall.order.dto.*;

/**
 */
public interface OrderQueryService {

    OrderListResponse02 queryOrder(Long uid);

    OrderDetailResponse queryOrderDetail(String orderId);

}
