package com.cskaoyan.gateway.controller.shopping;

/*
 * @description: 购物车相关
 * @author: wei
 * @date: 2022/1/19  22:24
 *
 */


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cskaoyan.gateway.form.shopping.CartForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ICartService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.*;
import com.mall.user.annotation.Anoymous;
import com.mall.user.intercepter.TokenIntercepter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("shopping")
public class CartController {

    @Reference(retries = 0, timeout = 3000, check = false)
    ICartService cartService;

    @GetMapping("carts")
    public ResponseData getCartList(HttpServletRequest request) {
        //1.获取请求request中token → 登录验证
        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());

        CartListByIdRequest cartListByIdRequest = new CartListByIdRequest();
        cartListByIdRequest.setUserId(uid);

        //2.调用service层 → 获取response
        CartListByIdResponse response = cartService.getCartListById(cartListByIdRequest);

        //3.返回结果
        if (ShoppingRetCode.SUCCESS.getCode().equals((response.getCode()))) {
            //返回包含购物车商品信息的ResponseData对象
            return new ResponseUtil().setData(response.getCartProductDtos());
        }
        //返回错误信息
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }


    @PostMapping("carts")
    public ResponseData addToCart(@RequestBody CartForm cartForm, HttpServletRequest request) {
        //1.获取请求request中token → 登录验证
        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());

        AddCartRequest addCartRequest = new AddCartRequest();
        addCartRequest.setUserId(uid);
        addCartRequest.setItemId(cartForm.getProductId());
        addCartRequest.setNum(cartForm.getProductNum());

        //调用service层 → 获取response
        AddCartResponse cartResponse = cartService.addToCart(addCartRequest);

        //返回结果
        if (ShoppingRetCode.SUCCESS.getCode().equals((cartResponse.getCode()))) {
            //返回包含购物车商品信息的ResponseData对象
            return new ResponseUtil().setData("成功");
        }
        //返回错误信息
        return new ResponseUtil().setErrorMsg(cartResponse.getMsg());
    }

    @PutMapping("carts")
    public ResponseData updateCart(@RequestBody CartForm cartForm, HttpServletRequest request) {
        //1.获取请求request中token → 登录验证
        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long uid = Long.parseLong(object.get("uid").toString());

        UpdateCartNumRequest updateRequest = new UpdateCartNumRequest();
        updateRequest.setChecked(cartForm.getChecked());
        updateRequest.setItemId(cartForm.getProductId());
        updateRequest.setUserId(uid);
        updateRequest.setNum(cartForm.getProductNum());

        //调用service层 → 获取response
        UpdateCartNumResponse updateResponse = cartService.updateCartNum(updateRequest);

        //返回结果
        if (ShoppingRetCode.SUCCESS.getCode().equals((updateResponse.getCode()))) {
            //返回包含购物车商品信息的ResponseData对象
            return new ResponseUtil().setData("成功");
        }
        //返回错误信息
        return new ResponseUtil().setErrorMsg(updateResponse.getMsg());

    }



    @RequestMapping("/carts/{uid}/{pid}")
    public ResponseData cartDeleteById( @PathVariable("uid") Long userId, @PathVariable("pid") Long itemId, HttpServletRequest servletRequest) {
        DeleteCartItemRequest request = new DeleteCartItemRequest();
        request.setItemId(itemId);
        request.setUserId(userId);

        DeleteCartItemResponse response = cartService.deleteCartItem(request);

        if (response.getCode().equals(ShoppingRetCode.SUCCESS.getCode())) {
            return new ResponseUtil().setData(response.getMsg());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

    @RequestMapping("/items")
    public ResponseData CheckAllItem(@RequestBody CheckAllItemRequest request, HttpServletRequest servletRequest) {
        CheckAllItemResponse response = cartService.checkAllCartItem(request);

        return new ResponseUtil().setData(response.getMsg());
    }


    @RequestMapping("/items/{uid}")
    public ResponseData DeleteCheckedItem( @PathVariable("uid") Long userId,HttpServletRequest servletRequest) {
        DeleteCheckedItemRequest request = new DeleteCheckedItemRequest();
        request.setUserId(userId);

        DeleteCheckedItemResposne resposne = cartService.deleteCheckedItem(request);

        return new ResponseUtil().setData(resposne.getMsg());
    }


}
