package com.cskaoyan.gateway.controller.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderCoreService;
import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dto.*;
import com.mall.user.annotation.Anoymous;
import com.mall.user.intercepter.TokenIntercepter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @description:此类用于订单模块请求的接收和处理
 * @time: 2022/1/20 10:34
 * @author: Zhao Jun
 */
@RestController
@RequestMapping("shopping")
public class OrderControllerOfZJ {

    @Reference(timeout = 3000,check = false)
    private OrderCoreService orderCoreService;

    @Reference(timeout = 3000,check = false)
    private OrderQueryService orderQueryService;

    /**
     * @description:此方法用于创建订单
     * @return: com.mall.commons.result.ResponseData
     * @date: 2022年1月20日15:46:17
     * @author ZhaoJun
     */
    @PostMapping("order")
    public ResponseData createOrder(@RequestBody CreateOrderRequest request, HttpServletRequest servletRequest){
        //获取uid
        String userInfo = ((String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY));
        JSONObject jsonObject = JSON.parseObject(userInfo);
        long uid = Long.parseLong(jsonObject.get("uid").toString());
        request.setUserId(uid);
        //设置uniqueKey
        request.setUniqueKey(UUID.randomUUID().toString());

        CreateOrderResponse response = orderCoreService.createOrder(request);
        if (response.getCode().equals(OrderRetCode.SUCCESS.getCode())){
            return new ResponseUtil<>().setData(response.getOrderId());
        }
        return new ResponseUtil<>().setErrorMsg(response.getMsg());
    }

    /**
     * @description:此方法用于查询用户的所有订单
     * @return: com.mall.commons.result.ResponseData
     * @date: 2022年1月20日17:00:47
     * @author ZhaoJun
     */
    @GetMapping("order")
    public ResponseData queryOrder(Integer size,Integer page, HttpServletRequest servletRequest){
        String userInfo = ((String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY));
        JSONObject jsonObject = JSON.parseObject(userInfo);
        long uid = Long.parseLong(jsonObject.get("uid").toString());
//        Long uid = ((Long) servletRequest.getAttribute("userInfo"));
        OrderListResponse02 orderListResponse = orderQueryService.queryOrder(uid);
        return new ResponseUtil<>().setData(orderListResponse);
    }

    /**
     * @description:此方法用于订单详情的查询
     * @return: com.mall.commons.result.ResponseData
     * @date: 2022年1月20日17:18:15
     * @author ZhaoJun
     */
    @GetMapping("order/{id}")
    public ResponseData queryOrderDetail(@PathVariable("id")String orderId){

        //从数据库查询出来的response
        OrderDetailResponse orderDetailResponse = orderQueryService.queryOrderDetail(orderId);

        //需要返回的response
        OrderDetailMsgResponse orderDetailMsgResponse = new OrderDetailMsgResponse();

        //给需要返回的response赋值
        orderDetailMsgResponse.setGoodsList(orderDetailResponse.getOrderItemDto());
        orderDetailMsgResponse.setUserName(orderDetailResponse.getBuyerNick());
        orderDetailMsgResponse.setOrderTotal(orderDetailResponse.getPayment().intValue());
        orderDetailMsgResponse.setUserId(orderDetailResponse.getUserId().intValue());
        orderDetailMsgResponse.setTel(orderDetailResponse.getOrderShippingDto().getReceiverMobile());
        orderDetailMsgResponse.setStreetName(orderDetailResponse.getOrderShippingDto().getReceiverAddress());
        orderDetailMsgResponse.setOrderStatus(orderDetailResponse.getStatus());

        if (orderDetailResponse.getCode().equals(OrderRetCode.SUCCESS.getCode())){
            return new ResponseUtil<>().setData(orderDetailMsgResponse);
        }
        return new ResponseUtil<>().setErrorMsg(orderDetailMsgResponse.getMsg());
    }
}
