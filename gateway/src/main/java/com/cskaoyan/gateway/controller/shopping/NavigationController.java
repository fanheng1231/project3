package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IContentService;
import com.mall.shopping.dto.NavListResponse;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName NavigationController
 * @Description
 * @Author FanHeng
 * @Date 2022/01/19 22:34
 **/
@Slf4j
@RestController
@RequestMapping("/shopping")
public class NavigationController {
    @Reference(timeout = 3000,retries = 2, check = false)
    IContentService contentService;

    /**
     * @author: Fanheng
     * @description: 查询导航栏的列表
     * @date: 2022/01/19 22:40
     * @param
     * @return
     */
    @GetMapping("/navigation")
    @Anoymous
    public ResponseData getNavigationList(){
        NavListResponse navListResponse = contentService.queryNavList();
        if (navListResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil().setData(navListResponse.getPannelContentDtos());
        }
        return new ResponseUtil().setErrorMsg(navListResponse.getMsg());
    }
}
