package com.cskaoyan.gateway.controller.user;

import com.cskaoyan.gateway.form.user.UserRegisterForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IKaptchaService;
import com.mall.user.UserRegisterService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.KaptchaCodeRequest;
import com.mall.user.dto.KaptchaCodeResponse;
import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * description: 用户注册
 * Creation: 2022/01/20 20:20
 *
 * @author: liuqiang@HyperSquirrel
 */
@RestController
@RequestMapping("user")
public class RegisterController {
    //获取验证码
    @Reference(timeout = 3000, check = false)
    IKaptchaService kaptchaService;

    //注册用户信息
    @Reference(timeout = 100000, check = false)
    UserRegisterService userRegisterService;

    @Anoymous
    @PostMapping("/register")
    public ResponseData userRegister(@RequestBody UserRegisterForm userRrgister,
                                  HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        KaptchaCodeRequest request = new KaptchaCodeRequest();
        //从cookie获取验证码的uuid
        String uuid = CookieUtil.getCookieValue(httpServletRequest, "kaptcha_uuid");
        request.setUuid(uuid);
        request.setCode(userRrgister.getCaptcha());
        //校验验证码和UUID是否为空
        request.requestCheck();
        //验证码是否正确
        KaptchaCodeResponse codeResponse = kaptchaService.validateKaptchaCode(request);
        if (!codeResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtil<>().setErrorMsg("验证码错误");
        }
        //封装远程调用的请求对象
        UserRegisterRequest registerRequest = new UserRegisterRequest();
        registerRequest.setUserName(userRrgister.getUserName());
        registerRequest.setUserPwd(userRrgister.getUserPwd());
        registerRequest.setEmail(userRrgister.getEmail());


        UserRegisterResponse response = userRegisterService.userRegisterService(registerRequest);
        if (response != null) {

            return new ResponseUtil<>().setData(response);
        }

        return new ResponseUtil<>().setErrorMsg("用户名或密码不正确");
    }
}
   
