package com.cskaoyan.gateway.controller.user;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IKaptchaService;
import com.mall.user.IVerifyService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.KaptchaCodeRequest;
import com.mall.user.dto.KaptchaCodeResponse;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/user")
public class CaptchaController {

    @Reference(timeout = 3000, check = false)
    IKaptchaService kaptchaService;


    @Reference(timeout = 3000, check = false)
    IVerifyService verifyService;
    /**
     * 获取验证码
     */
    @Anoymous
    @GetMapping("/kaptcha")
    public ResponseData getKaptchaCode(HttpServletResponse response) {
        KaptchaCodeRequest kaptchaCodeRequest = new KaptchaCodeRequest();

        KaptchaCodeResponse kaptchaCodeResponse = kaptchaService.getKaptchaCode(kaptchaCodeRequest);

        if (kaptchaCodeResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            Cookie cookie = CookieUtil.genCookie("kaptcha_uuid", kaptchaCodeResponse.getUuid(), "/", 60);
            cookie.setHttpOnly(true);
            response.addCookie(cookie);
            return new ResponseUtil<>().setData(kaptchaCodeResponse.getImageCode());
        }
        return new ResponseUtil<>().setErrorMsg(kaptchaCodeResponse.getCode());
    }

    @Anoymous
    @PostMapping("/kaptcha")
    public ResponseData validKaptchaCode(@RequestBody String code, HttpServletRequest httpServletRequest) {
        KaptchaCodeRequest request = new KaptchaCodeRequest();
        String uuid = CookieUtil.getCookieValue(httpServletRequest, "kaptcha_uuid");
        request.setUuid(uuid);
        request.setCode(code);
        KaptchaCodeResponse response = kaptchaService.validateKaptchaCode(request);
        if (response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtil<>().setData(null);
        }
        return new ResponseUtil<>().setErrorMsg(response.getCode());
    }

    /**
     * @Description: 退出登录
     * @Param:
     * @return:
     * @Author: liuWenJin
     * @Date: 2022/1/20
     */
    @GetMapping("loginOut")
    public ResponseData userLoginOut(HttpServletResponse httpServletResponse){


        //新建一个cookie，覆盖
//        Cookie cookie = new Cookie("access_token", null);
        Cookie cookie = CookieUtil.genCookie("access_token", null, "/", 3000000);
        httpServletResponse.addCookie(cookie);

        return new ResponseUtil<>().setData(null);
        //access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
        // eyJpc3MiOiJ3bGd6cyIsImV4cCI6MTY0Mjc1MzEwNywidXNlciI6IkQ2RkEzNEU2RjgzMDU2QTY4Q0Q1NzQ4RUFEM0JGQTk3MkE1QTFCN0ExNDc5NEUwNTk5RkU0OEM4OUYzOTAzQzgifQ
        // .gi9vDzeXVoZZemklKx3xvMh7Ils98alWrO5aLrgoQZc

    }



    @GetMapping("verify")
    public ResponseData userVerify(String uid,String username){

        UserVerifyRequest userVerifyRequest = new UserVerifyRequest();

        //封装
        userVerifyRequest.setUuid(uid);
        userVerifyRequest.setUserName(username);

        UserVerifyResponse userVerifyResponse = verifyService.userVerify(userVerifyRequest);

        //对返回值进行判断
        if (userVerifyResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil<>().setData(userVerifyResponse.getMsg());
        }

        return new ResponseUtil<>().setErrorMsg(userVerifyResponse.getCode());

    }

}
