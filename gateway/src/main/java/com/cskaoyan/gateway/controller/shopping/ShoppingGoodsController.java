package com.cskaoyan.gateway.controller.shopping;

import com.cskaoyan.gateway.form.shopping.GooodsForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductCateService;
import com.mall.shopping.IProductService;
import com.mall.shopping.dto.*;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ShoppingGoodsController
 * @Description 关于商品的操作
 * @Author FanHeng
 * @Date 2022/01/20 09:40
 **/
@Slf4j
@RestController
@RequestMapping("/shopping")
public class ShoppingGoodsController {

    @Reference(timeout = 3000,retries = 2, check = false)
    IProductService iProductService;

    @Reference(timeout = 3000,retries = 2, check = false)
    IProductCateService iProductCateService;
    /**
     * @author: Fanheng
     * @description: 获取所有的商品种类
     * @date: 2022/01/20 09:44
     * @param
     * @return
     */
    @GetMapping("/categories")
    @Anoymous
    public ResponseData getGoodsCategories(AllProductCateRequest request){
        AllProductCateResponse response = iProductCateService.getAllProductCate(request);
        if (response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil().setData(response.getProductCateDtoList());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

    @GetMapping("/product/{id}")
    @Anoymous
    public ResponseData getGoodsDetail(@PathVariable("id") Long id){
        ProductDetailRequest request = new ProductDetailRequest();
        request.setId(id);
        ProductDetailResponse response = iProductService.getProductDetail(request);
        if (response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil().setData(response.getProductDetailDto());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

    @GetMapping("/goods")
    @Anoymous
    public ResponseData getGoodsList(GooodsForm gooodsForm,String priceGt,String priceLte){
        Integer priceGtO = null;
        Integer priceLteO = null;
        if (priceGt!=null&&!priceGt.equals("")){
            priceGtO =  Integer.parseInt(priceGt);
        }
        if (priceLte!=null&&!priceLte.equals("")){
            priceLteO =  Integer.parseInt(priceLte);
        }
        AllProductRequest request = new AllProductRequest(gooodsForm.getPage(),gooodsForm.getSize(),
                gooodsForm.getSort(),gooodsForm.getCid(),priceGtO,priceLteO);
        AllProductResponse response = iProductService.getAllProduct(request);
        if (response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil().setData(response);
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

    @GetMapping("/recommend")
    @Anoymous
    public ResponseData getRecommendGoods(){
        RecommendResponse response = iProductService.getRecommendGoods();
        if (response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil().setData(response.getPanelContentItemDtos());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }
}
