package com.cskaoyan.gateway.controller.user;

import com.cskaoyan.gateway.form.user.UserLoginForm;
import com.cskaoyan.gateway.vo.CheckAuthVO;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IKaptchaService;
import com.mall.user.UserLoginService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.*;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * description: 用户登录的控制层
 * Creation: 2022/01/19 21:37
 * @author: liuqiang@HyperSquirrel
 */

@RestController
@RequestMapping("user")
public class LoginController {

    //获取验证码
    @Reference(timeout = 10000, check = false)
    IKaptchaService kaptchaService;

    //获取用户登录信息
    @Reference(timeout = 10000, check = false)
    UserLoginService userLoginService;

    @Anoymous
    @PostMapping("/login")
    public ResponseData userLogin(@RequestBody UserLoginForm userLogin,
                                  HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        KaptchaCodeRequest request = new KaptchaCodeRequest();
        //从cookie获取验证码的uuid
        String uuid = CookieUtil.getCookieValue(httpServletRequest, "kaptcha_uuid");
        request.setUuid(uuid);
        request.setCode(userLogin.getCaptcha());
        //校验验证码和UUID是否为空
        request.requestCheck();
        //验证 验证码正确
        KaptchaCodeResponse codeResponse = kaptchaService.validateKaptchaCode(request);
        if (!codeResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtil<>().setErrorMsg(codeResponse.getMsg());
        }
        //封装远程调用的请求对象
        UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setUserName(userLogin.getUserName());
        loginRequest.setPassword(userLogin.getUserPwd());

        UserLoginResponse userLoginResponse = userLoginService.userLogin(loginRequest);
        if (userLoginResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            Cookie cookie=CookieUtil.genCookie("access_token", userLoginResponse.getToken(),"/",600000);
            httpServletResponse.addCookie(cookie);
            return new ResponseUtil<>().setData(userLoginResponse);
        }

        return new ResponseUtil<>().setErrorMsg(userLoginResponse.getMsg());
    }




    @GetMapping("/login")
    public ResponseData userLogin(HttpServletRequest httpServletRequest) {
        CheckAuthRequest request = new CheckAuthRequest();
        //从cookie中取出access_token
        String accessToken = CookieUtil.getCookieValue(httpServletRequest, "access_token");
        request.setToken(accessToken);
        //校验Token格式
         request.requestCheck();
        //验证 Token
        CheckAuthResponse tokenResponse = userLoginService.validToken(request);
        //验证成功，返回用户信息
        if (tokenResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            CheckAuthVO checkAuthVO = new CheckAuthVO();
            checkAuthVO.setUid(tokenResponse.getUserId());
            checkAuthVO.setFile(tokenResponse.getFile());
            return new ResponseUtil<>().setData(checkAuthVO);

        }

        return new ResponseUtil<>().setErrorMsg(tokenResponse.getMsg());
    }
}
   
