package com.cskaoyan.gateway.controller.order;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderCancelService;
import com.mall.order.OrderDeleteService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;
import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;
import com.mall.user.annotation.Anoymous;
import lombok.extern.log4j.Log4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description:
 * @author:Daichuchu
 **/
@Log4j
@RestController
@RequestMapping("shopping")
public class OrderCancelController {
    @Reference(retries = 0,timeout = 50000,check = false)
    OrderCancelService orderCancelService;

    @Reference(retries = 0,timeout = 50000,check = false)
    OrderDeleteService orderDeleteService;

    @PostMapping("cancelOrder")
    @Anoymous
    public ResponseData cancelOrder(@RequestBody Map map){
        String orderId = (String) map.get("id");
        CancelOrderRequest request = new CancelOrderRequest();
        request.setOrderId(orderId);
        CancelOrderResponse response = orderCancelService.cancelOrder(request);
         if(OrderRetCode.SUCCESS.getCode().equals(response.getCode())){
             return new ResponseUtil().setData(response.getMsg());
         }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

        @RequestMapping(value = "order/{id}",method = RequestMethod.DELETE)
        @Anoymous
        public ResponseData deleteOrder(@PathVariable("id") String id){
            DeleteOrderRequest request = new DeleteOrderRequest();
            request.setOrderId(id);
            DeleteOrderResponse response = orderDeleteService.deleteOrder(request);
            if(OrderRetCode.SUCCESS.getCode().equals(response.getCode())){
                return new ResponseUtil().setData(response.getMsg());
            }
            return new ResponseUtil().setErrorMsg(response.getMsg());
        }
}
