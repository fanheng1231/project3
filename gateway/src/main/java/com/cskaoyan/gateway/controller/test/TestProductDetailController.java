package com.cskaoyan.gateway.controller.test;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ITestProductDetailService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.ITestProductDetailDto;
import com.mall.shopping.dto.ITestProductDetailRequest;
import com.mall.shopping.dto.ITestProductDetailResponse;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestProductDetailController {

    @Reference(retries = 0, timeout = 3000, check = false)
    ITestProductDetailService productDetailService;

    @GetMapping("/product")
    @Anoymous
    public ResponseData getProductDetail(Long productId) {
        //1.获取请求request
        ITestProductDetailRequest productDetailRequest = new ITestProductDetailRequest();
        productDetailRequest.setProductId(productId);
        //2.获取response
        ITestProductDetailResponse productResponse = productDetailService.getProductDetail(productDetailRequest);
        //3.返回结果
        if (ShoppingRetCode.SUCCESS.getCode().equals(productResponse.getCode())) {

            // 返回一个包含商品数据的ResponseData对象
            return new ResponseUtil().setData(productResponse.getProductDetailDto());
        }
        // 返回包含错误信息的ResponseData对象
        return new ResponseUtil().setErrorMsg(productResponse.getMsg());
    }
}
