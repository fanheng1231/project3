package com.cskaoyan.gateway.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: 验证用户信息
 * Creation: 2022/01/20 15:28
 *
 * @author: liuqiang@HyperSquirrel
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CheckAuthVO {
Long uid;
String file;
}
   
