package com.cskaoyan.gateway.form.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: 用户注册
 * Creation: 2022/01/20 20:15
 *
 * @author: liuqiang@HyperSquirrel
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRegisterForm {
    String userName;
    String userPwd;
    String captcha;
    String email;
}
   
