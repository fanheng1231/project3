package com.cskaoyan.gateway.form.shopping;

/*
 * @description: 添加商品到购物车、更新商品到购物车相关
 * @author: wei
 * @date: 2022/1/20  16:03
 *
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartForm {

    private Long userId;

    private Long productId;

    private Integer productNum;

    private String checked;

}
