package com.cskaoyan.gateway.form.shopping;

import lombok.Data;

/**
 * @ClassName GooodsForm
 * @Description
 * @Author FanHeng
 * @Date 2022/01/20 14:15
 **/
@Data
public class GooodsForm {
    private Integer page;
    private Integer size;
    private String sort; //为空是不排序，为1就是升序，为-1是降序
    private Long cid;
}
