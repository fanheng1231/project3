package com.cskaoyan.gateway.form.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: 用户登录提交的数据表单
 * Creation: 2022/01/19 20:56
 * @author: liuqiang@HyperSquirrel
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserLoginForm {

    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userPwd")
    private String userPwd;
    @JsonProperty("captcha")
    private String captcha;
}
   
