package com.mall.user.services;

import com.alibaba.fastjson.JSON;
import com.mall.commons.tool.exception.ValidateException;
import com.mall.user.UserRegisterService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.converter.UserConverterMapper;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * description:用户注册服务接口的实现类
 * Creation: 2022/01/20 20:25
 * @author: liuqiang@HyperSquirrel
 */
@Component
@Service
@Slf4j
public class UserRegisterServiceImpl implements UserRegisterService {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    MemberMapper memberMapper;
    @Autowired
    UserConverterMapper userConverterMapper;
    @Autowired
    UserVerifyMapper userVerifyMapper;

    @Override
    public UserRegisterResponse userRegisterService(UserRegisterRequest request)  {

        //校验请求参数是否为空
        request.requestCheck();

        //查询用户名，邮箱是否重复
        Example example=new Example(Member.class);
        example.createCriteria().orEqualTo("username", request.getUserName())
                .orEqualTo("password", request.getUserPwd());
        Integer count = memberMapper.selectCountByExample(example);
        if (count!=0)
        {
            throw new ValidateException(SysRetCodeConstants.USERNAME_ALREADY_EXISTS.getCode(),
                    SysRetCodeConstants.USERNAME_ALREADY_EXISTS.getMessage());
        }
        //获得系统时间
        Date date = new Date();
        SimpleDateFormat sdf =   new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = sdf.format(date);
        Date time=null;
        try {
             time= sdf.parse( nowTime );
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //对密码进行MD5加密
        String userPassword= DigestUtils.md5DigestAsHex(request.getUserPwd().getBytes());

        Member newMember = new Member();
        newMember.setUsername(request.getUserName());
        newMember.setPassword(userPassword);
        newMember.setEmail(request.getEmail());
        newMember.setCreated(time);
        newMember.setUpdated(time);
        newMember.setState(1);
        newMember.setIsVerified("N");

        //用户注册失败
        Integer affcetMemberRows = memberMapper.insert(newMember);
        if (affcetMemberRows!=1){
           UserRegisterResponse userRegisterResponse=new UserRegisterResponse();
           userRegisterResponse.setCode(SysRetCodeConstants.USER_REGISTER_FAILED.getCode());
           userRegisterResponse.setMsg(SysRetCodeConstants.USER_REGISTER_FAILED.getMessage());
            return  userRegisterResponse;
        }
        UserVerify userVerify=new UserVerify();
        userVerify.setUsername(newMember.getUsername());
        userVerify.setRegisterDate(newMember.getCreated());
        //生成UUID
        String key=newMember.getUsername()+newMember.getPassword()+ UUID.randomUUID().toString();
        String uuid=DigestUtils.md5DigestAsHex(key.getBytes());
        userVerify.setUuid(uuid);
        userVerify.setIsExpire("N");
        userVerify.setIsVerify("N");
        Integer affcetUserRows = userVerifyMapper.insert(userVerify);
        //用户验证表插入失败，返回提示
        if (affcetUserRows!=1){
            UserRegisterResponse userRegisterResponse=new UserRegisterResponse();
            userRegisterResponse.setCode(SysRetCodeConstants.USER_REGISTER_VERIFY_FAILED.getCode());
            userRegisterResponse.setMsg(SysRetCodeConstants.USER_REGISTER_VERIFY_FAILED.getMessage());
            return  userRegisterResponse;
        }
        //打印日志,会自动将xxx替换成{} 用户注册成功，注册参数request:
        log.info("用户注册成功，注册参数:{},{}", JSON.toJSONString(request),"xxx");
        sendEmail(uuid,request);

        UserRegisterResponse userRegisterResponse=new UserRegisterResponse();
        userRegisterResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        userRegisterResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return  userRegisterResponse;
    }
    //发送注册邮件
    private void sendEmail(String uuid,UserRegisterRequest userRegisterRequest){
        SimpleMailMessage message=new SimpleMailMessage();
        message.setSubject("CSMAll用户激活");
        message.setFrom("1019491614@qq.com");
        message.setTo(userRegisterRequest.getEmail());
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append("http://localhost:8080/user/verify?uid=").append(uuid)
                .append("&username=").append(userRegisterRequest.getUserName());
        message.setText(stringBuffer.toString());
        mailSender.send(message);
    }
}

   
