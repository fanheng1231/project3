package com.mall.user.services;

import com.mall.user.IVerifyService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

/*** 用户验证实现类
 * 创建日期: 2022/01/20 16:38 
 * @author liuWenJin */

@Component
@Service
public class IVerifyServiceImpl implements IVerifyService {


    @Autowired
    MemberMapper memberMapper;

    @Autowired
    UserVerifyMapper userVerifyMapper;


    @Override
    public UserVerifyResponse userVerify(UserVerifyRequest userVerifyRequest) {
        //先check
        userVerifyRequest.requestCheck();

        UserVerifyResponse userVerifyResponse = new UserVerifyResponse();


        //修改member
        Member member = new Member();
        member.setIsVerified("Y");
        Example example = new Example(Member.class);
        example.createCriteria()
                .andEqualTo("username",userVerifyRequest.getUserName());

        int i = memberMapper.updateByExampleSelective(member,example);
        if (i != 1){
            userVerifyResponse.setCode(SysRetCodeConstants.DB_EXCEPTION.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.DB_EXCEPTION.getMessage());
            return userVerifyResponse;
        }

        //修改userVerify
        UserVerify userVerify = new UserVerify();
        userVerify.setIsExpire("Y");
        userVerify.setIsVerify("Y");
        Example exampleVerify = new Example(UserVerify.class);
        exampleVerify.createCriteria()
                .andEqualTo("username",userVerifyRequest.getUserName())
                .andEqualTo("uuid",userVerifyRequest.getUuid());

        int j = userVerifyMapper.updateByExampleSelective(userVerify, exampleVerify);
        if (i != 1){
            userVerifyResponse.setCode(SysRetCodeConstants.DB_EXCEPTION.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.DB_EXCEPTION.getMessage());
            return userVerifyResponse;
        }

        userVerifyResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        userVerifyResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return userVerifyResponse;
    }
}
