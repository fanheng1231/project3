package com.mall.user.services;

import com.alibaba.fastjson.JSON;
import com.mall.user.UserLoginService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.converter.UserConverterMapper;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import com.mall.user.dto.CheckAuthRequest;
import com.mall.user.dto.CheckAuthResponse;
import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;
import com.mall.user.utils.JwtTokenUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * description: 用户登录服务接口的实现类
 * Creation: 2022/01/19 22:10
 *
 * @author: liuqiang@HyperSquirrel
 */
@Component
@Service
public class UserLoginServiceImpl implements UserLoginService {
    @Autowired
    MemberMapper memberMapper;
    @Autowired
    UserConverterMapper userConverterMapper;
    @Autowired
    UserVerifyMapper userVerifyMapper;

    @Override
    public UserLoginResponse userLogin(UserLoginRequest request) {
        Example example = new Example(Member.class);
        //对密码进行MD5加密
        String encryptedUserPassword= DigestUtils.md5DigestAsHex(request.getPassword().getBytes());
        example.createCriteria().andEqualTo("username", request.getUserName())
                .andEqualTo("password", encryptedUserPassword);
        List<Member> members = memberMapper.selectByExample(example);
        UserLoginResponse user = null;
        //数据库中没有查到用户信息，返回错误信息提示
        if (members == null) {
            UserLoginResponse userLoginResponse=new UserLoginResponse();
                userLoginResponse.setCode(SysRetCodeConstants.USERORPASSWORD_ERRROR.getCode());
                userLoginResponse.setMsg(SysRetCodeConstants.USERORPASSWORD_ERRROR.getMessage());
                return userLoginResponse;
        }
        //将DTO对象转换成DO对象
        for (Member member : members) {
            user = userConverterMapper.converter(member);
        }

        //验证用户是否激活
        UserVerify userVerify=new UserVerify();
        Example exampleUserVerify=new Example(UserVerify.class);
        exampleUserVerify.createCriteria().andEqualTo("username",user.getUsername());
        List<UserVerify> list=userVerifyMapper.selectByExample(exampleUserVerify);
        if (list.size()==1) {
            String verifyState=null;
            for (UserVerify verify : list) {
               verifyState =verify.getIsVerify();
            }
            UserLoginResponse userLoginResponse=new UserLoginResponse();
            if (verifyState=="N") {
                userLoginResponse.setCode(SysRetCodeConstants.USER_ISVERFIED_ERROR.getCode());
                userLoginResponse.setMsg(SysRetCodeConstants.USER_ISVERFIED_ERROR.getMessage());
               return userLoginResponse;
            }
        }

        Map<Long, String> userMap = new HashMap<>();
        userMap.put(user.getId(), user.getUsername());
        String tokenJsonData = JSON.toJSONString(userMap);

        //将用户id和用户姓名封装进Token
        JwtTokenUtils creatJwtTool = JwtTokenUtils.builder().msg(tokenJsonData).build();
        String s = creatJwtTool.creatJwtToken();
        user.setToken(s);
        user.setCode(SysRetCodeConstants.SUCCESS.getCode());
        user.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return user;
    }

    @Override
    public CheckAuthResponse validToken(CheckAuthRequest request) {

        //从请求中取出Token
        String tokenStr = request.getToken();
        JwtTokenUtils freeJwtTool = JwtTokenUtils.builder().token(tokenStr).build();
        String tokenData = freeJwtTool.freeJwt();

        //将Token中取出的Json字符串转Map并获取用户id和name
        Map userMap = (Map) JSON.parse(tokenData);

        String name = null;
        Long id = null;
        for (Object s : userMap.keySet()) {
            name = (String) userMap.get(s);
            id = Long.parseLong(String.valueOf(s));
        }

        //通过username查询用户信息
        Example example = new Example(Member.class);
        example.createCriteria().andEqualTo("username", name);
        List<Member> members = memberMapper.selectByExample(example);
        CheckAuthResponse checkAuth = new CheckAuthResponse();

        //将查询结果中的userid与Token中的userId比较，相等则通过验证
        if (members.size() == 1) {
            if (members.get(0).getId().equals(id)) {
                //将uid和username封装成Map，再转成Json字符串封装到CheckAuthResponse的Userinfo中
                Map<Object, Object> requestMap = new HashMap<>();
                requestMap.put("uid",members.get(0).getId());
                requestMap.put("username",members.get(0).getUsername());
                String tokenJsonData = JSON.toJSONString(requestMap);

                checkAuth.setUserId(members.get(0).getId());
                checkAuth.setUserInfo(tokenJsonData);
                checkAuth.setFile(members.get(0).getFile());
                checkAuth.setCode(SysRetCodeConstants.SUCCESS.getCode());
                checkAuth.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
                return checkAuth;
            }
        }

        checkAuth.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
        checkAuth.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
        return checkAuth;
    }
}
   
