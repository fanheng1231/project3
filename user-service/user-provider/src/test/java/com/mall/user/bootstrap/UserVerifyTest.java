package com.mall.user.bootstrap;

import com.alibaba.fastjson.JSON;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/***
 * 创建日期: 2022/01/20 17:16 
 * @author liuWenJin */

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserVerifyTest {


    @Autowired
    MemberMapper memberMapper;

    @Autowired
    UserVerifyMapper userVerifyMapper;

    @Test
    public void test() {
        String username = "cskaoyan01";

        String uuid = "3f3d929b89f45f730bf430d2aa6bf23d";

        Member member = new Member();
        member.setIsVerified("Y");
        Example example = new Example(Member.class);
        example.createCriteria()
                .andEqualTo("username", username);

        int i = memberMapper.updateByExampleSelective(member, example);

        System.out.println("i = " + i);

        //修改userVerify
        UserVerify userVerify = new UserVerify();
        userVerify.setIsExpire("Y");
        userVerify.setIsVerify("Y");
        Example exampleVerify = new Example(UserVerify.class);
        exampleVerify.createCriteria()
                .andEqualTo("username", username)
                .andEqualTo("uuid", uuid);

        int j = userVerifyMapper.updateByExampleSelective(userVerify, exampleVerify);
        System.out.println("j = " + j);

    }

    @Test
    public void testMap() {
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put(123456, "iuuwenjin");

        String s = JSON.toJSONString(objectObjectHashMap);
        Map userMap = (Map) JSON.parse(s);

        System.out.println("Map=" + objectObjectHashMap);
        System.out.println("Map转Json字符串=" + s);
        System.out.println("Json字符串转Map=" + userMap);

        System.out.println("增强for遍历");
        for (Object o : userMap.keySet()) {
            System.out.println(userMap.get(o));

            System.out.println("迭代器遍历");
            Iterator<Map.Entry<Object, Object>> it = userMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Object, Object> entry = it.next();
                System.out.println("key= " + entry.getKey() + " and value= " + entry.getValue());
            }
        }
    }
}
