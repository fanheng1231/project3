package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * description: 验证用户访问携带的Token是否正确
 * Creation: 2022/01/20 13:50
 *
 * @author: liuqiang@HyperSquirrel
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CheckAuthResponse extends AbstractResponse {
    String userInfo;
    Long userId;
    String file;
}
   
