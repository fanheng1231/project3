package com.mall.user;

import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;

/*** 用户验证
 * 创建日期: 2022/01/20 16:37 
 * @author liuWenJin */

public interface IVerifyService {
    //验证用户
    UserVerifyResponse userVerify(UserVerifyRequest userVerifyRequest);


}
