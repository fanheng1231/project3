package com.mall.user;


import com.mall.user.dto.*;

/**
 * description: 用户登录服务接口
 * Creation: 2022/01/19 21:32
 *
 * @author: liuqiang@HyperSquirrel
 */
public interface UserLoginService {
    UserLoginResponse userLogin(UserLoginRequest request);
     CheckAuthResponse validToken(CheckAuthRequest request);
}
