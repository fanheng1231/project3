package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

/**
 *  cskaoyan
 */
@Data
public class UserRegisterResponse extends AbstractResponse {
}
