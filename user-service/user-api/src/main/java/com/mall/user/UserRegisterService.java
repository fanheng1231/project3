package com.mall.user;

import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;

/**
 * description: 用户注册接口
 * Creation: 2022/01/20 20:23
 *
 * @author: liuqiang@HyperSquirrel
 */
public interface UserRegisterService {
    UserRegisterResponse userRegisterService(UserRegisterRequest request);
}
