package com.mall.shopping.bootstrap;


import com.mall.shopping.constant.GlobalConstants;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.ItemMapper;

import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.PanelContentItemDto;
import com.mall.shopping.dto.PanelDto;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MyTest {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ContentConverter contentConverter;

    //@Reference
    //ITestProductDetailService productDetailService;


    @Test
    public void testMapper() {
        Item item = itemMapper.selectByPrimaryKey(100023501);
        System.out.println(item);
    }

    @Test
    public void testService() {

    }
    @Test
    public void testGoodsList(){
        List<Item> items = itemMapper.selectItemFront(null, "price", "desc", 10, 100);
        System.out.println(items);
    }

    @Test
    public void  testRecommend(){
//        List<Panel> panels = panelMapper.selectPanelContentById(GlobalConstants.RECOMMEND_PANEL_ID);
//        System.out.println(panels);
//        Set<PanelDto> panelDtos = new HashSet<>();
//        for (Panel panel : panels) {
//            PanelDto panelDto = contentConverter.panen2Dto(panel);
//            List<PanelContentItem> panelContentItems = panel.getPanelContentItems();
//            List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
//            panelDto.setPanelContentItems(panelContentItemDtos);
//            panelDtos.add(panelDto);
//        }
//        System.out.println(panelDtos);

        List<Panel> panels = panelMapper.selectPanelList(GlobalConstants.RECOMMEND_PANEL_ID);
        System.out.println(panels);

        Set<PanelDto> panelDtos = contentConverter.pannelsToDtos(panels);
        System.out.println(panelDtos);

    }

}
