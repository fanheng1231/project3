package com.mall.shopping.bootstrap;

/*
 * @description:
 * @author: wei
 * @date: 2022/1/20  15:03
 *
 */

import com.mall.shopping.ICartService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.CartProductDto;
import com.mall.shopping.dto.DeleteCartItemResponse;
import com.mall.shopping.dto.DeleteCheckedItemRequest;
import com.mall.shopping.dto.DeleteCheckedItemResposne;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Set;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CartTest {

    @Autowired
    RedissonClient redissonClient;


    @Autowired
    ItemMapper itemMapper;


    @Autowired
    ICartService cartService;

    @Test
    public void test1() {
        RMap<Object, Object> user1 = redissonClient.getMap("1");
        CartProductDto product1 = new CartProductDto(100023501L, new BigDecimal(79), 5L, 100L, "true", "Smartisan 双口 & 快充车载充电器",
                "https://resource.smartisan.com/resource/d4480234a2f24b0ff5acd98288fd902d.jpg,https://resource.smartisan.com/resource/69ebf4ca620e6d5a1bb7cb54741e24d3.jpg,https://resource.smartisan.com/resource/214a422b7d250333bec4398d47eac601.jpg,https://resource.smartisan.com/resource/f512a3c4b97d204555f864d4aa17e7e9.jpg,https://resource.smartisan.com/resource/fc8a5d50ed260d9798cfba39ff5234d0.jpg");

        CartProductDto product2 = new CartProductDto(100040607L, new BigDecimal(2999), 5L, 100L, "false", "坚果 3",
                "https://resource.smartisan.com/resource/13e91511f6ba3227ca5378fd2e93c54b.png,https://resource.smartisan.com/resource/fac4130efc39ed4db697cc8d137890e9.png,https://resource.smartisan.com/resource/91dc3f577960e30ca11b632e7b6ebd0f.png,https://resource.smartisan.com/resource/61586b59793ac16bd973010aecad2ca9.png");
        user1.put(100023501L, product1);
        user1.put(100040607L, product2);

//        RMap<Integer, CartProductDto> userMap = redissonClient.getMap("1");

//        String checked = "true";
//        Long userId = 1L;
//
//
//        RMap<Object, Object> map = redissonClient.getMap(userId.toString());
//        map.remove(100040607L);
//        System.out.println();



//        RMap<Long, CartProductDto> map = redissonClient.getMap(userId.toString());
//        /**
//         * 从map中拿到所有的key
//         *
//         * 遍历key，用key取到相应的value
//         *
//         * 修改value中所有CartProductDto中的checked为传过来的值
//         *
//         */
//
//        Set<Long> integers = map.readAllKeySet();
//        for (Long longs : integers) {
//            CartProductDto cartProductDto = map.get(longs);
//            cartProductDto.setChecked(checked);
//            map.put(longs,cartProductDto);
//        }
//        System.out.println();
/*        Long l = 1L;
        RMap<Long, CartProductDto> map = redissonClient.getMap(l.toString());
        Set<Long> integers = map.readAllKeySet();
        for (Long longs : integers) {
            CartProductDto cartProductDto = map.get(longs);
            if ("true".equals(cartProductDto.getChecked())) {
                map.remove(longs);
            }
        }
        System.out.println(user1);*/

    }

    //取出redis中的所有数据
    @Test
    public void test2() {
        RMap<Long, CartProductDto> user1 = redissonClient.getMap("72");
        Set<Long> keySet = user1.keySet();
        for (Long productId : keySet) {
            System.out.println(productId + ": " + user1.get(productId));
        }
        System.out.println("完毕");
    }

    @Test
    public void test3() {

        Item item = itemMapper.selectByPrimaryKey(100023501);
        System.out.println(item);
    }

}
