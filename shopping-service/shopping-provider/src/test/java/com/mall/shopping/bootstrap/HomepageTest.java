package com.mall.shopping.bootstrap;

import com.mall.shopping.dal.persistence.HomepageMapper;
import com.mall.shopping.dto.PanelDto;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/***
 * 创建日期: 2022/01/20 11:04 
 * @author liuWenJin */

@SpringBootTest
@RunWith(SpringRunner.class)
public class HomepageTest {

    @Autowired
    HomepageMapper homepageMapper;

    @Test
    public void test(){

        List<PanelDto> panelDtos = homepageMapper.SelectHomePage();
        System.out.println(panelDtos);
    }
}
