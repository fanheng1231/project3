package com.mall.shopping.services;

import com.mall.shopping.IProductCateService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ProductCateConverter;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.persistence.ItemCatMapper;
import com.mall.shopping.dto.AllProductCateRequest;
import com.mall.shopping.dto.AllProductCateResponse;
import com.mall.shopping.dto.ProductCateDto;
import com.mall.shopping.utils.ExceptionProcessorUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName IProductCateServiceImpl
 * @Description 商品类目相关操作
 * @Author FanHeng
 * @Date 2022/01/20 10:30
 **/
@Slf4j
@Component
@Service
public class IProductCateServiceImpl implements IProductCateService {
    @Autowired
    ItemCatMapper itemCatMapper;

    @Autowired
    ProductCateConverter productCateConverter;
    @Override
    public AllProductCateResponse getAllProductCate(AllProductCateRequest request) {
        AllProductCateResponse response = new AllProductCateResponse();
        try {
            List<ItemCat> itemCats = itemCatMapper.selectAll();
            List<ProductCateDto> productCateDtos = productCateConverter.items2Dto(itemCats);
            response.setProductCateDtoList(productCateDtos);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("com.mall.shopping.services.IProductCateServiceImpl.getAllProductCate occur Exception :"+e);
            ExceptionProcessorUtils.wrapperHandlerException(response,e);
        }


        return response;
    }
}
