package com.mall.shopping.services;

/*
 * @description:
 * @author: wei
 * @date: 2022/1/20  10:08
 *
 */

import com.mall.shopping.ICartService;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
@Service
public class CartServiceImpl implements ICartService {

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    ItemMapper itemMapper;

    //获得购物车商品列表
    @Override
    public CartListByIdResponse getCartListById(CartListByIdRequest request) {

        CartListByIdResponse cartResponse = new CartListByIdResponse();

        try {
            //参数校验 (校验userId)
            request.requestCheck();
            Long userId = request.getUserId();

            //从redis中取出数据 (long → String)
            RMap<Long, CartProductDto> user1 = redissonClient.getMap(String.valueOf(userId));

            //将Map中的所有value值(每一个都是CartProductDto) → 转换成list<CartProductDto>
            Collection<CartProductDto> collection = user1.values();
            //将collection转换成list
            List<CartProductDto> values = new ArrayList<>();
            for (CartProductDto productDto : collection) {
                //只将第一张图片返回进行显示
                String imgArrays = productDto.getProductImg();
                String[] str = imgArrays.split(",");
                productDto.setProductImg(null);
                productDto.setProductImg(str[0]);

                values.add(productDto);
            }

            //执行成功 → 返回结果
            cartResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            cartResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            cartResponse.setCartProductDtos(values);
        } catch (Exception e) {
            //执行失败，打印异常日志
            e.printStackTrace();
            cartResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            cartResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return cartResponse;
    }


    //添加商品到购物车
    @Override
    public AddCartResponse addToCart(AddCartRequest request) {

        AddCartResponse addCartResponse = new AddCartResponse();

        try {
            //1.参数校验
            request.requestCheck();
            Long userId = request.getUserId();

            //2.获取该用户的购物车信息 (long → String)
            RMap<Long, CartProductDto> cartRMap = redissonClient.getMap(String.valueOf(userId));

            //3.首先判断用户输入的数量是否小于limitNum
            //先去tb_item表中根据id获取该商品
            Item item = itemMapper.selectByPrimaryKey(request.getItemId());
            //3.2 用户输入的数量不合理
            if (request.getNum() > item.getLimitNum()) {
                addCartResponse.setMsg("该商品库存不足");
                return addCartResponse;
            }

            //3.2 用户输入的数量合理
            //在productRMap查询新添加的商品Id，看是否原来已在购物车中
            //(1)如果是添加相同的productId商品 → 数量累加,直接返回
            if (cartRMap.containsKey(request.getItemId())) {
                CartProductDto productDto = cartRMap.get(request.getItemId());
                //修改购物车中该商品的数量
                Long preNum = productDto.getProductNum();
                Long curNum = preNum + request.getNum();
                productDto.setProductNum(curNum);
                cartRMap.put(request.getItemId(), productDto);   //更新Redis中该商品的数量
                return addCartResponse;
            }

            //(2)如果是添加不同的商品 → 向redis添加一条记录
            //向redis中添加一条记录
            CartProductDto productDto = new CartProductDto(request.getItemId(), item.getPrice(), request.getNum().longValue(), item.getLimitNum().longValue(), "true", item.getTitle(), item.getImageBig());
            cartRMap.put(request.getItemId(), productDto);

            //执行成功 → 返回结果
            addCartResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            addCartResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());

        } catch (Exception e) {
            //执行失败，打印异常日志
            e.printStackTrace();
            addCartResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            addCartResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return addCartResponse;
    }


    //更新购物车
    @Override
    public UpdateCartNumResponse updateCartNum(UpdateCartNumRequest request) {

        UpdateCartNumResponse response = new UpdateCartNumResponse();

        try {
            //参数校验 (校验userId)
            request.requestCheck();
            Long userId = request.getUserId();

            //从redis中取出数据 (long → String)
            RMap<Long, CartProductDto> cartMap = redissonClient.getMap(String.valueOf(userId));

            //检查更新的数量是否小于limitedNum
            //先去tb_item表中根据id获取该商品
            Item item = itemMapper.selectByPrimaryKey(request.getItemId());
            //1.用户输入的数量不合理
            if (request.getNum() > item.getLimitNum()) {
                response.setMsg("该商品库存不足");
                return response;
            }

            //2.输入的数量合理 → 更新cartMap中相应商品的数量、选中状态
            CartProductDto updateProduct = cartMap.get(request.getItemId());
            updateProduct.setProductNum(request.getNum().longValue());
            updateProduct.setChecked(request.getChecked());
            cartMap.put(request.getItemId(), updateProduct);

            //执行成功 → 返回结果
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());

        } catch (Exception e) {
            //执行失败，打印异常日志
            e.printStackTrace();
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return response;

    }


    @Override
    public CheckAllItemResponse checkAllCartItem(CheckAllItemRequest request) {
        Long userId = request.getUserId();
        String checked = request.getChecked();
        CheckAllItemResponse response = new CheckAllItemResponse();
        RMap<Long, CartProductDto> map = redissonClient.getMap(userId.toString());
        /**
         * 从map中拿到所有的key
         *
         * 遍历key，用key取到相应的value
         *
         * 修改value中所有CartProductDto中的checked为传过来的值
         *
         */

        Set<Long> integers = map.readAllKeySet();
        for (Long longs : integers) {
            CartProductDto cartProductDto = map.get(longs);
            cartProductDto.setChecked(checked);
            map.put(longs, cartProductDto);
        }

        response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        return response;
    }

    @Override
    public DeleteCartItemResponse deleteCartItem(DeleteCartItemRequest request) {
        DeleteCartItemResponse response = new DeleteCartItemResponse();
        Long itemId = request.getItemId();
        Long userId = request.getUserId();

        RMap<Long, CartProductDto> map = redissonClient.getMap(String.valueOf(userId));
        map.remove(itemId);
        response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        return response;
    }

    @Override
    public DeleteCheckedItemResposne deleteCheckedItem(DeleteCheckedItemRequest request) {
        DeleteCheckedItemResposne resposne = new DeleteCheckedItemResposne();
        Long userId = request.getUserId();

        RMap<Long, CartProductDto> map = redissonClient.getMap(userId.toString());
        Set<Long> integers = map.readAllKeySet();
        for (Long longs : integers) {
            CartProductDto cartProductDto = map.get(longs);
            if ("true".equals(cartProductDto.getChecked())) {
                map.remove(longs);
            }
        }
        resposne.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        resposne.setCode(ShoppingRetCode.SUCCESS.getCode());
        return resposne;
    }

    @Override
    public ClearCartItemResponse clearCartItemByUserID(ClearCartItemRequest request) {
        ClearCartItemResponse response = new ClearCartItemResponse();

        Long userId = request.getUserId();

        List<Long> productIds = request.getProductIds();

        RMap<Long, CartProductDto> map = redissonClient.getMap(userId.toString());
        for (Long productId : productIds) {
            CartProductDto cartProductDto = map.get(productId);
            if ("true".equals(cartProductDto.getChecked())){
                map.remove(productId);
            }
        }
        response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        return response;
    }

}
