package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dto.ITestProductDetailDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ITestProductDetailConverter {

    @Mappings({
            @Mapping(source = "title", target = "name"),
            @Mapping(source = "image", target = "imgUrl")
    })
    ITestProductDetailDto productDetailDoToDto(Item item);
}
