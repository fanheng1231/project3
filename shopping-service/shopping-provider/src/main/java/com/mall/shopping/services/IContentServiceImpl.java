package com.mall.shopping.services;

import com.mall.shopping.IContentService;
import com.mall.shopping.constant.GlobalConstants;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dto.NavListResponse;
import com.mall.shopping.dto.PanelContentDto;
import com.mall.shopping.utils.ExceptionProcessorUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @ClassName IContentServiceImpl
 * @Description
 * @Author FanHeng
 * @Date 2022/01/19 22:12
 **/
@Slf4j
@Component
@Service
public class IContentServiceImpl implements IContentService {
    @Autowired
    PanelContentMapper panelContentMapper;

    @Autowired
    ContentConverter contentConverter;


    @Override
    public NavListResponse queryNavList() {

        NavListResponse navListResponse = new NavListResponse();

        try {
            Example example = new Example(PanelContent.class);
            example.createCriteria().andEqualTo("panelId", GlobalConstants.HEADER_PANEL_ID);
            List<PanelContent> panelContents = panelContentMapper.selectByExample(example);
            List<PanelContentDto> panelContentDtos = contentConverter.panelContents2Dto(panelContents);
            navListResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            navListResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            navListResponse.setPannelContentDtos(panelContentDtos);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("com.mall.shopping.services.IContentServiceImpl.queryNavList occur Exception :"+e);
            ExceptionProcessorUtils.wrapperHandlerException(navListResponse,e);
        }

        return navListResponse;
    }
}
