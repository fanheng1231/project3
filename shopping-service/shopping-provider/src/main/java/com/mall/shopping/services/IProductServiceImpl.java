package com.mall.shopping.services;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.shopping.IProductService;
import com.mall.shopping.constant.GlobalConstants;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.*;
import com.mall.shopping.utils.ExceptionProcessorUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @ClassName IProductServiceImpl
 * @Description
 * @Author FanHeng
 * @Date 2022/01/20 10:39
 **/
@Slf4j
@Component
@Service
public class IProductServiceImpl implements IProductService {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    ProductConverter productConverter;

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ContentConverter contentConverter;


    @Override
    public ProductDetailResponse getProductDetail(ProductDetailRequest request) {
        ProductDetailResponse response = new ProductDetailResponse();
        try {
            request.requestCheck();
            Item item = itemMapper.selectByPrimaryKey(request.getId());
            //先转换
            ProductDetailDto productDetailDto = productConverter.item2DetailDto(item);
            productDetailDto.setDetail("");
            productDetailDto.setProductImageBig(item.getImageBig());
            String[] images = item.getImages();
            List<String> imgList = new ArrayList<>();
            for (String image : images) {
                imgList.add(image);
            }
            productDetailDto.setProductImageSmall(imgList);
            response.setProductDetailDto(productDetailDto);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("com.mall.shopping.services.IProductServiceImpl.getProductDetail occur Exception :"+e);
            ExceptionProcessorUtils.wrapperHandlerException(response,e);
        }

        return response;
    }

    @Override
    public AllProductResponse getAllProduct(AllProductRequest request) {
        AllProductResponse response = new AllProductResponse();
        try {
            request.requestCheck();
            Integer page = request.getPage(); //当前页
            Integer limit = request.getSize(); //一页数量
            Integer priceGt = request.getPriceGt();
            Integer priceLte = request.getPriceLte();
            String sort = request.getSort();
            Long cid = request.getCid();
            PageHelper.startPage(page,limit);
            List<Item> itemList = itemMapper.selectItemFront(cid, "price", sort, priceGt, priceLte);
            List<ProductDto> productDtos = productConverter.items2Dto(itemList);
            PageInfo<Item> itemPageInfo = new PageInfo<>(itemList);
            long total = itemPageInfo.getTotal();
            response.setData(productDtos);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
            log.error("com.mall.shopping.services.IProductServiceImpl.getAllProduct occur Exception :"+e);
            ExceptionProcessorUtils.wrapperHandlerException(response,e);
        }


        return response;
    }

    @Override
    public RecommendResponse getRecommendGoods() {
        RecommendResponse response = new RecommendResponse();

        try {
            List<Panel> panels = panelMapper.selectPanelList(GlobalConstants.RECOMMEND_PANEL_ID);
            Set<PanelDto> panelDtos = contentConverter.pannelsToDtos(panels);
            response.setPanelContentItemDtos(panelDtos);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("com.mall.shopping.services.IProductServiceImpl.getRecommendGoods occur Exception :"+e);
            ExceptionProcessorUtils.wrapperHandlerException(response,e);
        }

        return response;
    }
}
