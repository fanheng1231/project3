package com.mall.shopping.services;

import com.mall.shopping.ITestProductDetailService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ITestProductDetailConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.ITestProductDetailDto;
import com.mall.shopping.dto.ITestProductDetailRequest;
import com.mall.shopping.dto.ITestProductDetailResponse;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ITestProductDetailServiceImpl implements ITestProductDetailService {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    ITestProductDetailConverter productDetailConverter;

    @Override
    public ITestProductDetailResponse getProductDetail(ITestProductDetailRequest request) {
        System.out.println("getProductDetail 被调用了");
        ITestProductDetailResponse productDetailResponse = new ITestProductDetailResponse();

        try {
            // 参数校验
            request.requestCheck();
            Item item = itemMapper.selectByPrimaryKey(request.getProductId());
            // 对象转化
            ITestProductDetailDto iTestProductDetailDto = productDetailConverter.productDetailDoToDto(item);

            // 执行成功
            productDetailResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            productDetailResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            productDetailResponse.setProductDetailDto(iTestProductDetailDto);
        } catch (Exception e) {
            e.printStackTrace();
            productDetailResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            productDetailResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return productDetailResponse;
    }
}
