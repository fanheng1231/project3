package com.mall.shopping.dal.persistence;

import com.mall.shopping.dto.PanelDto;

import java.util.List;

/*** 主页显示
 * 创建日期: 2022/01/19 21:12 
 * @author liuWenJin */

public interface HomepageMapper {

    List<PanelDto> SelectHomePage();
}
