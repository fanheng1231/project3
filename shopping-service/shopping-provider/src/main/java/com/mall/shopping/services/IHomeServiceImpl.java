package com.mall.shopping.services;

import com.mall.shopping.IHomeService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dal.persistence.HomepageMapper;
import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.dto.PanelDto;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

/*** 显示商品主页实现类
 * 创建日期: 2022/01/19 21:07 
 * @author liuWenJin */

@Service
public class IHomeServiceImpl implements IHomeService {

    @Autowired
    HomepageMapper homepageMapper;
    @Override
    public HomePageResponse homepage() {

        //它继承了Response，所以除了HomePageResponse中的成员变量，还有一些其他 d
        HomePageResponse homePageResponse = new HomePageResponse();

        try {
            List<PanelDto> panelDtos = homepageMapper.SelectHomePage();

            homePageResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            homePageResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
            homePageResponse.setPanelContentItemDtos(panelDtos);
        }catch (Exception e){
            e.printStackTrace();
            homePageResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            homePageResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return homePageResponse;

    }
}
