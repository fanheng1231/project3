package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

@Data
public class ITestProductDetailResponse extends AbstractResponse {

    private ITestProductDetailDto productDetailDto;
}
