package com.mall.shopping.dto;

import com.mall.commons.result.AbstractRequest;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.Data;

import javax.xml.bind.ValidationException;

@Data
public class ITestProductDetailRequest extends AbstractRequest {

    private Long productId;

    @Override
    public void requestCheck() throws ValidationException {
        if (productId == null || productId < 0) {
            throw new ValidationException(ShoppingRetCode.PARAMETER_VALIDATION_FAILED.getMessage(),
                    ShoppingRetCode.PARAMETER_VALIDATION_FAILED.getCode());
        }
    }
}
