package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 *  cskaoyan
 */


/**
 * @Description: 主页显示
 * @Param:
 * @return:
 * @Author: liuWenJin
 * @Date: 2022/1/19
 */
@Data
public class HomePageResponse extends AbstractResponse {

    private List<PanelDto> panelContentItemDtos;
}
