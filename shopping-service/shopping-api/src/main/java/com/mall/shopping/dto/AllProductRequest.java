package com.mall.shopping.dto;

import com.mall.commons.result.AbstractRequest;
import com.mall.commons.tool.exception.ValidateException;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * cskaoyan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AllProductRequest extends AbstractRequest {

    private Integer page;
    private Integer size;
    private String sort; //为空是不排序，为1就是升序，为-1是降序
    private Long cid;
    private Integer priceGt;
    private Integer priceLte;

    @Override
    public void requestCheck() {
        if (page <= 0) {
            setPage(1);
        }

        if (size < 0) {
            setSize(20);
        }

        if (sort.equals("1")) {
            setSort("asc");
        } else if (sort.equals("-1")) {
            setSort("desc");
        } else {
            setSort("desc");
        }

        if (cid != null && cid < 0){
            throw new ValidateException(ShoppingRetCode.REQUEST_CHECK_FAILURE.getCode(),
                    ShoppingRetCode.REQUEST_CHECK_FAILURE.getMessage());
        }
        if (priceGt != null && priceGt < 0){
            throw new ValidateException(ShoppingRetCode.REQUEST_CHECK_FAILURE.getCode(),
                    ShoppingRetCode.REQUEST_CHECK_FAILURE.getMessage());
        }
        if (priceLte != null && priceLte < 0){
            throw new ValidateException(ShoppingRetCode.REQUEST_CHECK_FAILURE.getCode(),
                    ShoppingRetCode.REQUEST_CHECK_FAILURE.getMessage());
        }
    }
}
