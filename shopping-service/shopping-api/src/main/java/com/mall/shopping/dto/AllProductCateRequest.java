package com.mall.shopping.dto;


import com.mall.commons.result.AbstractRequest;
import com.mall.commons.tool.exception.ValidateException;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.Data;

/**
 * Created on 2019/8/8
 * 21:46.
 */
@Data
public class AllProductCateRequest extends AbstractRequest {
    private String sort;

    @Override
    public void requestCheck() {
        if (!(sort.equals("asc")||sort.equals("desc"))){
            throw new ValidateException(
                    ShoppingRetCode.REQUEST_CHECK_FAILURE.getCode(),
                    ShoppingRetCode.REQUEST_CHECK_FAILURE.getMessage());
        }
    }
}
