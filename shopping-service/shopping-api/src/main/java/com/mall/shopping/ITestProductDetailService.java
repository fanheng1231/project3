package com.mall.shopping;

import com.mall.shopping.dto.ITestProductDetailDto;
import com.mall.shopping.dto.ITestProductDetailRequest;
import com.mall.shopping.dto.ITestProductDetailResponse;

public interface ITestProductDetailService{
    ITestProductDetailResponse getProductDetail(ITestProductDetailRequest request);
}
