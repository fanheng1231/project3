package com.mall.shopping.dto;

import com.mall.commons.result.AbstractRequest;
import com.mall.commons.tool.exception.ValidateException;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.Data;

/**
 *  cskaoyan
 */
@Data
public class ProductDetailRequest extends AbstractRequest {

    private Long id;

    @Override
    public void requestCheck() {
        if(id==null||id<0){
            throw new ValidateException(ShoppingRetCode.REQUEST_CHECK_FAILURE.getCode(),ShoppingRetCode.REQUEST_CHECK_FAILURE.getMessage());
        }
    }
}
